Proyecto Realizado con Express y Mongodb

Instrucciones para ejecutar el proyecto:
1. Instalar las siguientes dependencias que se encuentran en el archivo package.json
mediante el comando npm install 
2. Para ejecutar el proyecto utilizar el comando npm run dev(debe haber instalado nodemon mediante el comando npm install) 
3. Existen archivos de pruebas en la carpeta files
nota: debe tener instalado mogoDB para poder usar la base la de datos
4. Consultar en la base de datos con la consola de mongo los documentos generados por cada archivo subido.
utilizando los comandos: use archivos-db, una vez conectado a la base de datos con el comando anterior, ejecutar el comando db.archivos.find().pretty()

